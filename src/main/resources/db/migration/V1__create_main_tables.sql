CREATE TABLE users (
    user_id bigint PRIMARY KEY auto_increment,
    name varchar (30) NOT NULL,
    username varchar (15) NOT NULL,
    email varchar (40) NOT NULL,
    password varchar(100) NOT NULL
);

CREATE TABLE roles (
    role_id bigint PRIMARY KEY auto_increment,
    role_name VARCHAR(50) NOT NULL
);

CREATE TABLE user_roles (
    role_id bigint NOT NULL,
    user_id bigint NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (role_id) REFERENCES roles (role_id)
);
INSERT INTO roles(role_name) VALUES('ROLE_USER');
INSERT INTO roles(role_name) VALUES('ROLE_ADMIN');

INSERT INTO USERS(NAME, USERNAME, EMAIL, PASSWORD) VALUES
('Narqize','Narqize','narqize@gmail.com','$2y$12$npumeKnUPa5a.DHh3ys5s.RB/ZOlpC75P37wDHtfUfxXOQVUJqy6m');

INSERT INTO user_roles(user_id,role_id) VALUES (LAST_INSERT_ID(),2);

CREATE TABLE Patients(
    patient_id bigint not null AUTO_INCREMENT,
    full_name varchar (50) NOT NULL,
    birthday DATE ,
    gender varchar(20),
    number varchar (20),
    email varchar (255),
    address varchar(255),
    medical_alert varchar(255),
    PRIMARY KEY(patient_id)
);

CREATE TABLE appointments(
    appointment_id bigint PRIMARY KEY auto_increment,
    user_id bigint NOT NULL,
    patient_id bigint NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (patient_id) REFERENCES Patients(patient_id),
    date DATE NOT NULL,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP
);








