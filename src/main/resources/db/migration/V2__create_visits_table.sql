CREATE TABLE Visits(
    visit_id bigint PRIMARY KEY auto_increment,
    diagnose varchar(255),
    description varchar (255),
    paid decimal(10,2),
    date DATE,
    tooth_number integer,
    is_baby_tooth integer
);

CREATE TABLE Treatments(
    treatment_id bigint PRIMARY KEY auto_increment,
    code INTEGER ,
    description varchar (255),
    visit_id bigint DEFAULT NULL,
    FOREIGN KEY (visit_id) REFERENCES Visits(visit_id)
);

