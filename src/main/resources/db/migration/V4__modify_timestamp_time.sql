ALTER TABLE appointments
MODIFY start_time time;

ALTER TABLE appointments
MODIFY end_time time;