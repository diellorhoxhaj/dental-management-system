package com.dental.management.controller;

import com.dental.management.dto.PatientDto;
import com.dental.management.dto.TreatmentDto;
import com.dental.management.dto.VisitDto;
import com.dental.management.entity.Treatment;
import com.dental.management.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TreatmentController {

    @Autowired
    private TreatmentService treatmentService;

    @GetMapping(value = "/treatments")
    public List<TreatmentDto> findAll(){
        return treatmentService.findAll();
    }

    @PostMapping(value = "/treatment")
    public TreatmentDto save(@RequestBody @Valid TreatmentDto treatmentDto){
        return treatmentService.insert(treatmentDto);
    }

}
