package com.dental.management.controller;

import com.dental.management.dto.VisitDto;
import com.dental.management.dto.VisitRequestBody;
import com.dental.management.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class VisitController {

    @Autowired
    private VisitService visitService;

    @GetMapping(value = "/visits")
    public List<VisitDto> findAll(){
        return visitService.findAll();
    }

    @PostMapping(value = "visit")
    public VisitDto save(@RequestBody @Valid VisitRequestBody visitRequestBody){
        return visitService.insert(visitRequestBody);
    }
    @GetMapping(value = "/visit/{id}")
    public VisitDto find(@PathVariable Long id) throws Exception{
        return visitService.find(id);
    }

}
