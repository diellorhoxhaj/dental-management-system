package com.dental.management.controller;

import com.dental.management.dto.AppointmentDto;
import com.dental.management.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @GetMapping(value = "/appointments")
    public List<AppointmentDto> findAll(){
        return appointmentService.findAll();
    }

    @PostMapping(value = "/appointment")
    public AppointmentDto save(@RequestBody @Valid AppointmentDto appointmentDto){
        return appointmentService.insert(appointmentDto);
    }
}
