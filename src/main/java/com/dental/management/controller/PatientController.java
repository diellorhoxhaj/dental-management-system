package com.dental.management.controller;

import com.dental.management.dto.PatientDto;
import com.dental.management.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;

@RestController
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping(value = "/patients")
    public List<PatientDto> findAll(){
        return patientService.findAll();
    }

    @PostMapping(value = "/patient")
    public PatientDto save(@RequestBody @Valid PatientDto patientDto){
        return patientService.insert(patientDto);
    }

    @PostMapping(value = "/updatePatient")
    public PatientDto update(@RequestBody @Valid PatientDto patientDto){
        return patientService.update(patientDto);
    }
}
