package com.dental.management.entity;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private Long id;

    @Column(name = "diagnose")
    private String diagnose;

    @Column(name = "description")
    private String description;

    @Column(name = "paid")
    private BigDecimal paid;

    @Column(name = "date")
    private Date date;

    @Column(name = "tooth_number")
    private int toothNumber;

    @Column(name = "is_baby_tooth")
    private boolean isBabyTooth;

    //By default the fetch type is lazy, lazy loading it will fetch only when we want (use get method of list)
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "visit")
    private List<Treatment> treatmentList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public Visit(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getToothNumber() {
        return toothNumber;
    }

    public void setToothNumber(int toothNumber) {
        this.toothNumber = toothNumber;
    }

    public boolean getBabyTooth() {
        return isBabyTooth;
    }

    public void setBabyTooth(boolean babyTooth) {
        isBabyTooth = babyTooth;
    }

    public List<Treatment> getTreatmentList() {
        return treatmentList;
    }

    public void setTreatmentList(List<Treatment> treatmentList) {
        this.treatmentList = treatmentList;
    }

    public boolean getIsBabyTooth() {
        return isBabyTooth;
    }

    public void setIsBabyTooth(boolean isBabyTooth) {
        this.isBabyTooth = isBabyTooth;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        patient.addVisit(this);
        this.patient = patient;
    }

}
