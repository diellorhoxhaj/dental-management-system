package com.dental.management.service;

import com.dental.management.dao.PatientDao;
import com.dental.management.dao.TreatmentDao;
import com.dental.management.dao.VisitDao;
import com.dental.management.dto.VisitDto;
import com.dental.management.dto.VisitRequestBody;
import com.dental.management.entity.Patient;
import com.dental.management.entity.Treatment;
import com.dental.management.entity.Visit;
import com.dental.management.mapper.TreatmentMapper;
import com.dental.management.mapper.VisitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {

    @Autowired
    private VisitDao visitDao;

    @Autowired
    private VisitMapper visitMapper;

    @Autowired
    private TreatmentMapper treatmentMapper;

    @Autowired
    private TreatmentDao treatmentDao;

    @Autowired
    private PatientDao patientDao;


    @Transactional
    @Override
    public VisitDto insert(VisitRequestBody visitRequestBody) {
        Patient patient = patientDao.findByFullName(visitRequestBody.getPatientFullName());
        List<Treatment> treatmentsList = treatmentDao.findAllById(visitRequestBody.getTreatmentIds());

        Visit visit = visitMapper.mapResponseBodyToEntity(visitRequestBody);
        visit.setPatient(patient);

        treatmentsList.forEach(t -> {
            visit.getTreatmentList().add(t);
            t.setVisit(visit);
        });
//        for(Treatment treatment : treatmentsList){
//            visit.getTreatmentList().add(treatment);
//        }
        Visit updatedVisit = visitDao.save(visit);
        return visitMapper.mapToDto(updatedVisit);
    }

    @Override
    public VisitDto find(Long id) {
        Visit visit = visitDao.findById(id).get();
        if(visit == null){
            throw new RuntimeException("Not found");
        }
        return visitMapper.mapToDto(visit);

    }

    @Override
    public List<VisitDto> findAll() {
        return visitMapper.mapToDtoList(visitDao.findAll());
    }

}
