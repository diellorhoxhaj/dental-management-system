package com.dental.management.service;

import com.dental.management.dao.PatientDao;
import com.dental.management.dto.PatientDto;
import com.dental.management.entity.Patient;
import com.dental.management.mapper.PatientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientDao patientDao;

    @Autowired
    private PatientMapper mapper;

    @Override
    public PatientDto insert(PatientDto patientDto) {
        Patient patient = mapper.mapToEntity(patientDto);
        Patient patientFromDbs = patientDao.findByFullNameAndNumber(patientDto.getFullName(),patientDto.getNumber());
        if(patientFromDbs != null){
            throw new RuntimeException("Patient exists");
        }
        Patient savedPatient = patientDao.save(patient);
        if (savedPatient == null) {
            throw new RuntimeException("Patient Doesn't exist");
        }
        return mapper.mapToDto(savedPatient);
    }

    @Override
    public List<PatientDto> findAll() {
        return mapper.mapToDtoList(patientDao.findAll());
    }

    public List<PatientDto> findByFullName(String fullName) {
        List<Patient> patients = patientDao.findPatientsByFullName(fullName.toLowerCase());
        if (patients == null) {
            throw new RuntimeException("Patient Doesn't exist");
        }
        return mapper.mapToDtoList(patients);
    }

    @Override
    public PatientDto update(PatientDto patient) {
        Patient patientToUpdate = patientDao.findById(patient.getId()).get();
        if(patientToUpdate == null){
            throw new RuntimeException("Patient Doesn't exist");
        }
        patientToUpdate.setNumber(patient.getNumber());
        patientToUpdate.setMedicalAlert(patient.getMedicalAlert());
        patientToUpdate.setGender(patient.getGender());
        patientToUpdate.setFullName(patient.getFullName());
        patientToUpdate.setAddress(patient.getAddress());
        patientToUpdate.setBirthday(patient.getBirthday());
        patientToUpdate.setEmail(patient.getEmail());

        patientDao.save(patientToUpdate);

        return mapper.mapToDto(patientToUpdate);
    }
}
