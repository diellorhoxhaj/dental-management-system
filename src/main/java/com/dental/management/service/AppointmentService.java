package com.dental.management.service;

import com.dental.management.dto.AppointmentDto;

import java.util.List;

public interface AppointmentService {
    AppointmentDto insert(AppointmentDto patient);
    List<AppointmentDto> findAll();
}
