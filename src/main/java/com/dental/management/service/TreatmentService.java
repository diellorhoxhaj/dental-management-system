package com.dental.management.service;

import com.dental.management.dto.TreatmentDto;

import java.util.List;

public interface TreatmentService {
    TreatmentDto insert(TreatmentDto treatmentDto);

    List<TreatmentDto> findAll();

}
