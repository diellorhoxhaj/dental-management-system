package com.dental.management.service;

import com.dental.management.dao.UserDao;
import com.dental.management.dto.UserDto;
import com.dental.management.entity.User;
import com.dental.management.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements  UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    UserMapper userMapper;

    @Override
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new RuntimeException(username+ " doesn't exist");
        }
        return user;
    }
}
