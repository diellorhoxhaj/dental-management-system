package com.dental.management.service;

import com.dental.management.dao.AppointmentDao;
import com.dental.management.dao.PatientDao;
import com.dental.management.dto.AppointmentDto;
import com.dental.management.dto.PatientDto;
import com.dental.management.entity.Appointment;
import com.dental.management.entity.Patient;
import com.dental.management.mapper.AppointmentMapper;
import com.dental.management.mapper.PatientMapper;
import com.dental.management.util.PatientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentDao appointmentDao;

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private PatientUtil patientUtil;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public AppointmentDto insert(AppointmentDto appointmentDto) {
        Patient patient = patientUtil.getPatientByNameAndPhoneNumber(appointmentDto.getPatientFullName(),appointmentDto.getPhoneNumber());
        Appointment newAppointment = appointmentMapper.mapToEntity(appointmentDto);
        newAppointment.setPatient(patient);
        newAppointment.setUser(userService.findByUsername(appointmentDto.getUsername()));
        Appointment updatedAppointment = appointmentDao.save(newAppointment);
        return appointmentMapper.mapToDto(updatedAppointment);
    }
    @Override
    public List<AppointmentDto> findAll() {
        List<Appointment> appointments = new ArrayList<>();
        appointmentDao.findAll().forEach(appointments::add);
        return appointmentMapper.mapToDtoList(appointments);
    }
}
