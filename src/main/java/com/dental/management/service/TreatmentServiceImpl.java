package com.dental.management.service;

import com.dental.management.dao.TreatmentDao;
import com.dental.management.dto.TreatmentDto;
import com.dental.management.entity.Treatment;
import com.dental.management.mapper.TreatmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class TreatmentServiceImpl implements TreatmentService
{
    @Autowired
    TreatmentMapper treatmentMapper;

    @Autowired
    TreatmentDao treatmentDao;

    @Override
    public TreatmentDto insert(TreatmentDto treatmentDto) {
        Treatment treatment = treatmentMapper.mapToEntity(treatmentDto);
        treatmentDao.save(treatment);
        return treatmentMapper.mapToDto(treatment);
    }
    @Override
    public List<TreatmentDto> findAll() {
        return treatmentMapper.mapToDtoList(treatmentDao.findAll());
    }
}
