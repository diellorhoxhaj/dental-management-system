package com.dental.management.service;

import com.dental.management.dto.VisitDto;
import com.dental.management.dto.VisitRequestBody;

import java.util.List;

public interface VisitService  {
    List<VisitDto> findAll();
    VisitDto insert(VisitRequestBody visitRequestBody);
    VisitDto find(Long id);

}
