package com.dental.management.service;

import com.dental.management.dto.PatientDto;
import com.dental.management.entity.Patient;

import java.util.List;

public interface PatientService {
     PatientDto insert(PatientDto patient);
     List<PatientDto> findAll();
     List<PatientDto> findByFullName(String fullName);
     PatientDto update(PatientDto patient);
}
