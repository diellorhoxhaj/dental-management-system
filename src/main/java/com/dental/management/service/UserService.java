package com.dental.management.service;

import com.dental.management.dto.UserDto;
import com.dental.management.entity.User;


public interface UserService {
    User findByUsername(String username);
}
