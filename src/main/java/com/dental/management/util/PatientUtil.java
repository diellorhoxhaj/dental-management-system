package com.dental.management.util;

import com.dental.management.dao.PatientDao;
import com.dental.management.dto.PatientDto;
import com.dental.management.entity.Patient;
import com.dental.management.mapper.PatientMapper;
import com.dental.management.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PatientUtil {

    @Autowired
    private PatientService patientService;

    @Autowired
    private PatientDao patientDao;

    @Autowired
    private PatientMapper patientMapper;

    public Patient getPatientByNameAndPhoneNumber(String fullName,String phoneNumber) {
        Patient patient = patientDao.findByFullNameAndNumber(fullName,phoneNumber);
        return patient == null ? savePatient(fullName,phoneNumber) : patient;
    }

    private Patient savePatient(String fullName,String phoneNumber) {
        PatientDto patientDto = new PatientDto();
        patientDto.setFullName(fullName);
        patientDto.setNumber(phoneNumber);
       return patientDao.save(patientMapper.mapToEntity(patientDto));
    }

}
