package com.dental.management.dto;

import com.dental.management.entity.Treatment;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VisitDto {

    private Long id;

    @Size(max = 50)
    private String diagnose;

    @Size(max = 200)
    private String description;

    private BigDecimal paid;

    @NotEmpty
    private String patientFullName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date date;

    private int toothNumber;

    private boolean isBabyTooth;

    private List<TreatmentDto> treatmentDtoList = new ArrayList<>();

    public VisitDto(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getToothNumber() {
        return toothNumber;
    }

    public void setToothNumber(int toothNumber) {
        this.toothNumber = toothNumber;
    }



    public boolean isBabyTooth() {
        return isBabyTooth;
    }

    public void setBabyTooth(boolean babyTooth) {
        isBabyTooth = babyTooth;
    }

    public List<TreatmentDto> getTreatmentDtoList() {
        return treatmentDtoList;
    }

    public void setTreatmentDtoList(List<TreatmentDto> treatmentDtoList) {
        this.treatmentDtoList = treatmentDtoList;
    }

    public String getPatientFullName() {
        return patientFullName;
    }

    public void setPatientFullName(String patientFullName) {
        this.patientFullName = patientFullName;
    }

}
