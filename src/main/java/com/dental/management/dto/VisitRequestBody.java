package com.dental.management.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class VisitRequestBody {
    private Long id;

    @Size(max = 50)
    private String diagnose;

    @Size(max = 200)
    private String description;

    private BigDecimal paid;

    @NotEmpty
    private String patientFullName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date date;

    private int toothNumber;

    private boolean isBabyTooth;

    private List<Long> treatmentIds = new ArrayList<>();

    public VisitRequestBody(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getToothNumber() {
        return toothNumber;
    }

    public void setToothNumber(int toothNumber) {
        this.toothNumber = toothNumber;
    }

    public boolean getIsBabyTooth() {
        return isBabyTooth;
    }

    public void setIsBabyTooth(boolean isBabyTooth) {
        this.isBabyTooth = isBabyTooth;
    }

    public boolean isBabyTooth() {
        return isBabyTooth;
    }

    public void setBabyTooth(boolean babyTooth) {
        isBabyTooth = babyTooth;
    }

    public List<Long> getTreatmentIds() {
        return treatmentIds;
    }

    public void setTreatmentIds(List<Long> treatmentIds) {
        this.treatmentIds = treatmentIds;
    }

    public String getPatientFullName() {
        return patientFullName;
    }

    public void setPatientFullName(String patientFullName) {
        this.patientFullName = patientFullName;
    }
}
