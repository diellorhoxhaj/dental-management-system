package com.dental.management.dto;

import com.dental.management.entity.Appointment;
import com.dental.management.entity.Visit;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class PatientDto {
    private Long id;

    @NotEmpty
    @Size(max = 20)
    private String fullName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date birthday;

    @Size(max = 20)
    private String gender;

    @Size(max = 20)
    @NotEmpty
    private String number;

    @Email
    private String email;

    @Size(max = 100)
    private String address;

    @Size(max = 100)
    private String medicalAlert;

    private List<AppointmentDto> appointmentList = new ArrayList<>();

    private List<VisitDto> visitList = new ArrayList<>();

    public PatientDto(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalAlert() {
        return medicalAlert;
    }

    public void setMedicalAlert(String medicalAlert) {
        this.medicalAlert = medicalAlert;
    }

    public List<AppointmentDto> getAppointmentList() {
        return appointmentList;
    }

    public void setAppointmentList(List<AppointmentDto> appointmentList) {
        this.appointmentList = appointmentList;
    }

    public List<VisitDto> getVisitList() {
        return visitList;
    }

    public void setVisitList(List<VisitDto> visitList) {
        this.visitList = visitList;
    }
}
