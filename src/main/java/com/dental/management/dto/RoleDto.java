package com.dental.management.dto;

import com.dental.management.entity.Role;
import com.dental.management.entity.RoleName;

public class RoleDto {
    private Long id;

    private RoleName name;

    public RoleDto(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleName getName() {
        return name;
    }

    public void setName(RoleName name) {
        this.name = name;
    }
}
