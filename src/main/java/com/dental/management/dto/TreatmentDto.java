package com.dental.management.dto;

import com.dental.management.entity.Visit;

import javax.validation.constraints.Size;
import java.util.Objects;

public class TreatmentDto {

    private Long id;

    private int code;

    @Size(max = 200)
    private String description;

    public TreatmentDto(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
