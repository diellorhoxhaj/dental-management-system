package com.dental.management.mapper;

import com.dental.management.dto.VisitDto;
import com.dental.management.dto.VisitRequestBody;
import com.dental.management.entity.Visit;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Component
public class VisitMapper implements CustomMapper<Visit, VisitDto> {

    @Autowired
    private TreatmentMapper treatmentMapper;



    @Override
    public VisitDto mapToDto(Visit visit) {
        VisitDto visitDto = new VisitDto();
        visitDto.setId(visit.getId());
        visitDto.setDate(visit.getDate());
        visitDto.setDescription(visit.getDescription());
        visitDto.setDiagnose(visit.getDiagnose());
        visitDto.setBabyTooth(visit.getBabyTooth());
        visitDto.setPaid(visit.getPaid());
        visitDto.setToothNumber(visit.getToothNumber());
        visitDto.setPatientFullName(visit.getPatient().getFullName());
        visitDto.setTreatmentDtoList(treatmentMapper.mapToDtoList(visit.getTreatmentList()));

        return visitDto;

    }

    @Override
    public Visit mapToEntity(VisitDto visitDto) {
        Visit visit = new Visit();
        visit.setId(visitDto.getId());
        visit.setDate(visitDto.getDate());
        visit.setDescription(visitDto.getDescription());
        visit.setDiagnose(visitDto.getDiagnose());
        visit.setBabyTooth(visitDto.isBabyTooth());
        visit.setPaid(visitDto.getPaid());

        return visit;
    }
    public List<VisitDto> mapToDtoList(Iterable<Visit> visits){
        List<VisitDto> visitDtos = new ArrayList<>();
        for(Visit visit : visits){
            visitDtos.add(mapToDto(visit));
        }
        return visitDtos;
    }

    public Visit mapResponseBodyToEntity(VisitRequestBody visitRequestBody){
        Visit visit = new Visit();
        BeanUtils.copyProperties(visitRequestBody,visit);
        return visit;
    }




}
