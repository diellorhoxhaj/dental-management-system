package com.dental.management.mapper;

import com.dental.management.dto.AppointmentDto;
import com.dental.management.dto.PatientDto;
import com.dental.management.entity.Appointment;
import com.dental.management.entity.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PatientMapper implements CustomMapper<Patient, PatientDto> {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private VisitMapper visitMapper;

    @Override
    public PatientDto mapToDto(Patient patient) {
        PatientDto patientDto = new PatientDto();
        patientDto.setId(patient.getId());
        patientDto.setAddress(patient.getAddress());
        patientDto.setAppointmentList(appointmentMapper.mapToDtoList(patient.getAppointmentList()));
        patientDto.setBirthday(patient.getBirthday());
        patientDto.setEmail(patient.getEmail());
        patientDto.setFullName(patient.getFullName());
        patientDto.setGender(patient.getGender());
        patientDto.setNumber(patient.getNumber());
        patientDto.setMedicalAlert(patient.getMedicalAlert());
        patientDto.setVisitList(visitMapper.mapToDtoList(patient.getVisitList()));

        return patientDto;
    }

    @Override
    public Patient mapToEntity(PatientDto patientDto) {
        Patient patient = new Patient();
        patient.setId(patientDto.getId());
        patient.setAddress(patientDto.getAddress());
        patient.setAppointmentList(appointmentMapper.mapToEntityList(patientDto.getAppointmentList()));
        patient.setBirthday(patientDto.getBirthday());
        patient.setEmail(patientDto.getEmail());
        patient.setFullName(patientDto.getFullName());
        patient.setGender(patientDto.getGender());
        patient.setMedicalAlert(patientDto.getMedicalAlert());
     //   patient.setVisitList(visitMapper.mapToDtoList(patientDto.getVisitList()));
        patient.setNumber(patientDto.getNumber());
        return patient;

    }
    public List<PatientDto> mapToDtoList(Iterable<Patient> patients){
        List<PatientDto> patientDtoList = new ArrayList<>();
        for(Patient patient : patients){
            patientDtoList.add(mapToDto(patient));
        }
        return patientDtoList;
    }
}
