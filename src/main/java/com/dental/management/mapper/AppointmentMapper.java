package com.dental.management.mapper;

import com.dental.management.dto.AppointmentDto;
import com.dental.management.entity.Appointment;
import com.dental.management.entity.User;
import com.dental.management.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppointmentMapper implements CustomMapper<Appointment, AppointmentDto> {

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PatientService patientService;

    @Override
    public AppointmentDto mapToDto(Appointment appointment) {
        AppointmentDto appointmentDto = new AppointmentDto();
        appointmentDto.setId(appointment.getId());
        appointmentDto.setDate(appointment.getDate());
        appointmentDto.setStartTime(appointment.getStartTime());
        appointmentDto.setEndTime(appointment.getEndTime());
        appointmentDto.setPatientFullName(appointment.getPatient().getFullName());
        appointmentDto.setPhoneNumber(appointment.getPatient().getNumber());
       // appointmentDto.setPatient(patientMapper.mapToDto(appointment.getPatient()));

        appointmentDto.setUsername(appointment.getUser().getName());

        return appointmentDto;

    }

    public List<AppointmentDto> mapToDtoList(Iterable<Appointment> appointmentList){
        List<AppointmentDto> appointmentDtoList = new ArrayList<>();
        for(Appointment appointment : appointmentList){
            appointmentDtoList.add(mapToDto(appointment));
        }
        return appointmentDtoList;
    }


    @Override
    public Appointment mapToEntity(AppointmentDto appointmentdto) {
        Appointment appointment = new Appointment();
        appointment.setId(appointmentdto.getId());
        appointment.setDate(appointmentdto.getDate());
        appointment.setStartTime(appointmentdto.getStartTime());
        appointment.setEndTime(appointmentdto.getEndTime());
        //appointment.setPatientFullName(appointmentdto.getPatientFullName());
       // appointment.setPatient(patientMapper.mapToEntity(appointmentdto.getPatient()));


        return appointment;
    }
    public List<Appointment> mapToEntityList(Iterable<AppointmentDto> appointmentDtoList){
        List<Appointment> appointmentList = new ArrayList<>();
        for(AppointmentDto appointment : appointmentDtoList){
            appointmentList.add(mapToEntity(appointment));
        }
        return appointmentList;
    }
}
