package com.dental.management.mapper;

public interface CustomMapper<Entity,DataTObject> {
    DataTObject mapToDto(Entity entity);
    Entity mapToEntity(DataTObject dto);
}

