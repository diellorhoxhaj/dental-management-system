package com.dental.management.mapper;

import com.dental.management.dto.TreatmentDto;
import com.dental.management.entity.Treatment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TreatmentMapper implements CustomMapper<Treatment,TreatmentDto>{
    public List<Treatment> mapToEntiyList(Iterable<TreatmentDto> treatments){
        List<Treatment> treatmentList = new ArrayList<>();
        for(TreatmentDto treatment : treatments){
            treatmentList.add(mapToEntity(treatment));
        }
        return treatmentList;
    }
    public List<TreatmentDto> mapToDtoList(Iterable<Treatment> treatments){
        List<TreatmentDto> treatmentList = new ArrayList<>();
        for(Treatment treatment : treatments){
            treatmentList.add(mapToDto(treatment));
        }
        return treatmentList;
    }

    @Override
    public TreatmentDto mapToDto(Treatment treatment) {
        TreatmentDto treatmentDto = new TreatmentDto();
        treatmentDto.setId(treatment.getId());
        treatmentDto.setCode(treatment.getCode());
        treatmentDto.setDescription(treatment.getDescription());

        return treatmentDto;
    }

    @Override
    public Treatment mapToEntity(TreatmentDto treatmentDto) {
        Treatment treatment = new Treatment();
        treatment.setCode(treatmentDto.getCode());
        treatment.setId(treatmentDto.getId());
        treatment.setDescription(treatmentDto.getDescription());

        return treatment;
    }
}
