package com.dental.management.mapper;

import com.dental.management.dto.UserDto;
import com.dental.management.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements CustomMapper<User, UserDto> {

    @Override
    public UserDto mapToDto(User user) {
        return null;
    }

    @Override
    public User mapToEntity(UserDto dto) {
        return null;
    }
}
