package com.dental.management.dao;

import com.dental.management.entity.Patient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PatientDao extends CrudRepository<Patient,Long> {
    Patient findByFullNameAndNumber(String fullName, String address);
    Optional<Patient> findById(Long id); //mundet mos me ekzistu Optional ti menjanon nullpointerexp
    List<Patient> findPatientsByFullName(String fullName);
    Patient findByFullName(String fullName);
}
