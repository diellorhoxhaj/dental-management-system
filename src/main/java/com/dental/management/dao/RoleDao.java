package com.dental.management.dao;

import com.dental.management.entity.Role;
import com.dental.management.entity.RoleName;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<Role,Long> {
    Role findRoleByName(RoleName roleName);
}
