package com.dental.management.dao;

import com.dental.management.entity.Appointment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AppointmentDao extends CrudRepository<Appointment, Long> {

    Appointment findByPatientFullName(String fullName);
    List<Appointment> findAppointmentsByPatientFullName(String fullName);


    //  Appointment findByPatient(Patient patient);
    //  List<Appointment> findAppointmentsByPatient(Patient patient);
    //  List<Appointment> findAppointmentsByPatientFullName(String fullName);
}
