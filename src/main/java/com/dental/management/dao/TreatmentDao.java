package com.dental.management.dao;

import com.dental.management.entity.Treatment;
import com.sun.xml.bind.v2.model.core.ID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TreatmentDao extends JpaRepository<Treatment,Long> {

}
