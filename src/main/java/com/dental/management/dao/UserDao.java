package com.dental.management.dao;

import com.dental.management.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User,Long> {
        User findByUsername(String username);
}
