package com.dental.management.dao;

import com.dental.management.entity.Patient;
import com.dental.management.entity.Visit;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VisitDao extends CrudRepository<Visit,Long> {
    List<Visit> findVisitsByPatient (Patient patient);
    Visit findVisitByPatient (Patient patient);

    List<Visit> findVisitsByPatientFullName(String fullName);



}
